# E-Gesetzgebung
Das Projekt „Elektronisches Gesetzgebungsverfahren des Bundes“ (EGesetzgebung) ist Bestandteil des vom Bundesministerium des Innern und für Heimat (BMI) verantworteten Programms “Dienstekonsolidierung Bund”. Sie soll das Gesetzgebungsverfahren des Bundes von der Erstellung von Entwürfen über die Abstimmung und Einbringung in den Deutschen Bundestag und Bundesrat bis hin zur Übergabe an die EVerkündung und das Neue Rechtsinformationssystem des Bundes vollständig digital, medienbruchfrei und interoperabel abbilden (siehe untenstehende Graphik zum Rechtsetzungskreislauf). 

![Rechtsetzungskreislauf der E-Gesetzgebung](images/egesetzgebung-rechtsetzungsprozess.png)

_Abbildung: Rechtsetzungskreislauf der E-Gesetzgebung_

Die E-Gesetzgebung gliedert sich in die Produkte Bundesregierungsplattform, Editor, Bundestagsplattform, Bundesratsplattform und Gesetzgebungsportal.  

Die **Bundesregierungsplattform** ist die Umgebung für die Anwendungen zur elektronischen und medienbruchfreien Abbildung des Rechtsetzungsverfahrens für die Bundesregierung. Diese umfasst die elektronische Erarbeitung, Kommentierung und Abstimmung von Regelungsentwürfen bis hin zur Zuleitung zum Bundeskabinett. Mithilfe des webbasierten **Editors** der E-Gesetzgebung können Regelungsentwürfe sequenziell, medienbruchfrei und rechtsförmlich erstellt, bearbeitet und kommentiert werden. Die **Bundestagsplattform** ermöglicht unter anderem das Erstellen, Einbringen und Annehmen von Vorlagen, das Anlegen und Verwalten von Bundestags- und Ausschussdrucksachen, die Vor- und Nachbereitung der Beratung dieser Drucksachen im Plenum und in den Ausschüssen des Deutschen Bundestages. Die **Bundesratsplattform** ermöglicht es, Vorlagen der Verfassungsorgane zu erhalten und relevante Informationen für die weitere Verwendung in bundesratsspezifischen Prozessen und Dokumenten zu extrahieren, beteiligte Ausschüsse in einem Umlaufverfahren zu ermitteln sowie vorbereitende Drucksachen und Ergebnisdokumente zu erstellen. Das im Koalitionsvertrag von 2021 verankerte **Gesetzgebungsportal** soll als Einstiegsportal eine zentrale Informationsquelle für Bürgerinnen und Bürger darstellen, die sich zu laufenden Regelungsvorhaben informieren möchten.  

Der Inhaltsdatenstandard LegalDocML.de für die elektronische Abbildung der Gesetzgebung wird von der IT-Maßnahme E-Gesetzgebung seit Juni 2020 als Grundlage zur Erstellung und Bearbeitung von Rechtsetzungsdokumenten genutzt. Ziel des Inhaltsdatenstandards ist es, den rechtsförmlichen Aufbau und die Struktur von Regelungstexten maschinenlesbar abzubilden sowie deren Bearbeitung und Weitergabe prozessübergreifend zu ermöglichen. Seit April 2024 wird der Inhaltsdatenstandard durch die Koordinierungsstelle für IT-Standards (KoSIT) betrieben und weiterentwickelt. 

Die E-Gesetzgebung wird agil entwickelt und neue Funktionalitäten werden stufenweise in halbjährlichen Releases bereitgestellt. Die Vollversion der E-Gesetzgebung ist lediglich in den Netzen des Bundes und nach Registrierung verfügbar. Für die Öffentlichkeit ist ein Blick hinter die Kulissen möglich. Mit ausgewählten Funktionen zum Ausprobieren stehen in der EGesetzgebung im Internet die Anwendungen Vorbereitung, Zeitplanung, Nachhaltigkeitsprüfung, Verfahrensassistent und Arbeitshilfenbibliothek unter https://plattform.egesetzgebung.bund.de/cockpit/#/cockpit zur Verfügung. 

Die Veröffentlichung auf Open CoDE fördert digitale Souveränität, indem Entwicklungen der E-Gesetzgebung transparent und für föderale und supranationale Stellen sowie interessierte Dritte nachnutzbar gemacht werden. Zurzeit bietet das Repository der E-Gesetzgebung den Programmcode der EGesetzgebung sowie den Inhaltsdatenstandard LegalDocML.de in der Version 1.7.1 an. 

Weitere Informationen und Metadaten, inklusive einer ausführlicheren Beschreibung des Projekts, können Sie der publiccode.yml, sowie der Homepages des Projekts im Internet unter [https://egesetzgebung.bund.de/](https://egesetzgebung.bund.de/) entnehmen.


### Nutzung von Docker (work in progress)
Um das Projekt in Docker (Compose) auszuführen, folgen Sie den Anweisungen in [der Docker Installations Anleitung](./Docker.md).

**ACHTUNG**: Diese Funktionalität befindet sich noch in der Entwicklung und bietet noch keine lauffähige Umgebung!


## Lizenz
Copyright (C) 2021-2024 Bundesministerium des Innern und für Heimat, Referat DG II 6, Maßnahmen Enterprise Resource Management und Elektronische Verwaltungsarbeit

Quellcode von der Applikation Plattform: MPL2.0

Beispieldateien, Validierungsregeln und Spezifikation: CC-BY 3.0 DE

(die Lizenztexte entnehmen Sie der jeweiligen LICENSE Datei)
