OpenCode: Installation der Plattform



## Backend



### Kompilierung



Es wird eine aktuelle Maven-Installation benötigt sowie Java 11 (AdoptOpenJDK) benötigt. Um ein Artefakt zu erhalten kann mvn package bzw. kurz mvn -P release -DskipTests ausgeführt werden. Mit dem zweiten Befehl werden die Tests übersprungen und das Profil release verwendet, welches irrelevante Abhängigkeiten für ein Deployment auf dem JBoss ausschließt (bspw. Embedded Tomcat). Fehlende Abhängigkeiten werden aus dem zentralen Maven Repository herunterladen, sodass eine Internetverbindung vorteilhaft ist.



### Voraussetzungen für das Deployment



Um die Anwendung erfolgreich deployen zu können, sind ein paar Voraussetzungen zu erfüllen. Die Konfiguration muss in der application.properties vorgenommen werden bzw. sie kann im JBoss über die System Properties geändert werden. Zu konfigurierende Properties sind durch ein XXXXX markiert, wobei nicht alle Einstellungen für ein erfolgreiches Deployment vorzunehmen sind. Systeme wie NeuRIS sind für den Start nicht erforderlich und führen, wenn sie nicht konfiguriert sind, erst beim Ausführen der Bestandssuche zu Fehlern. Für eine minimale Installation wurden die zwingend erforderlichen Konfigurationsabschnitte entsprechend markiert.



- **Zwingend:** Es wird eine MySQL-Datenbank benötigt. Die Standardkodierung kann übernommen werden.
CREATE DATABASE `eGesetz` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */



- **Zwingend:** Um die Datenbank anzulegen gibt es zwei mögliche Wege:


  - Per Init-Skript


  - Per Flyway: Standardmäßig ist Flyway ausgeschaltet. Um es einzuschalten muss man die Property “spring.flyway.enable=true” in dem application.properties-File setzen. Danach werden die Flyway-Skripte automatisch beim Start der Anwendung ausgeführt.


- **Zwingend:** Die Verbindung muss in den Properties eingetragen werden: spring.datasource.url, spring.datasource.username, spring.datasource.password


- **Zwingend:** Es muss die RBAC-Datenbank angelegt und initialisiert werden. Die Properties sind entsprechend anzupassen: auth.datasource.url, auth.datasource.username, auth.datasource.password


- **Zwingend:** Es wird ein Keycloak (IAM) benötigt. Das Keycloak muss den Client “egg” und das Realm “bund” eingerichtet haben. Die URL zum Keycloak sowie das dazugehörige Secret sind in den Properties einzutragen: spring.security.oauth2.client.registration.keycloak.client-secret, iam.auth.url


- **Zwingend:** Im Keycloak müssen User eindeutig über die E-Mail-Adresse identifiziert werden können. Zudem benötigt jeder User eine eindeutige Globale ID (kurz GID), das in dem JW Token mitgeliefert wird. Dazu ist für Attribut “gid” vom Typ String ein Mapping im Keycloak einzurichten. Weitere Informationen wie Anrede oder Telefonnummer können ebenfalls enthalten sein, sind allerdings optional.


- **Zwingend:** Es wird eine Anbindung an die Ressortdaten und Usersuche benötigt. Diese sind für den Start der Anwendung derzeit erforderlich. Es wird dafür ein weiteres Secret zum IAM benötigt, welches in den Properties eingetragen werden muss: iam.auth.client.secret


Es wird standardmäßig unter /var/plateg ein Verzeichnis mit Schreibzugriff erwartet. Das CMS Light muss unter /var/plateg/cms-light liegen. Das Verzeichnis kann in den Properties bei Bedarf angepasst werden: plateg.storage.dir



Es wird für Zustellungen von Regelungsvorhaben ans PKP eine entsprechende Instanz benötigt. Die URL und der API Key sind in den Properties zu hinterlegen: pkp.url, pkp.x-api-key

Bei Bedarf kann zusätzlich ein Proxy angegeben werden:proxy.default.host, proxy.default.port



Damit PKP Daten an uns zurückschreiben kann (ehemals “Rückkanal”), wird von PKP ein API Key benötigt: rv.x-api-key



Die URL zum eigenen System muss in den Properties hinterlegt werden:backend.url.base



Die URL zum Editor sowie der API Key muss in den Properties hinterlegt werden.



Der Mailserver kann für ausgehenden Mailverkehr konfiguriert werden.



Für die Bestandssuche muss NeuRIS konfiguriert werden: neuris.url, neuris.auth.username, neuris.auth.password

Bei Bedarf kann zusätzlich ein Proxy angegeben werden: proxy.default2.host, proxy.default2.port



### Ausführung



Das Deployment kann für lokale Entwicklungszwecke mit einem Embedded Tomcat erfolgen. Dies kann mit mvn spring-boot:run erfolgen. Die Anwendung sollte anschließend unter  verfügbar sein. Für einen Remotebetrieb ist ein JBoss EAP 7.2 oder 7.4 mit mindestens 2 GB RAM und Java 11 vorgesehen.



Es ist vorgesehen, dass Frontend und Backend auf dem selben System deployt werden.



## Frontend



Um Es wird eine aktuelle LTS-Version von Node.js benötigt (>= v16.x). Zusätzlich wird eine aktuelle Maven-Installation benötigt.



### Kompilierung



Um ein lauffähiges Artefakt (WAR) mit allen Teilen des Frontends bauen zu können, müssen einige vorbereitende Schritte vollzogen werden.



Alle Teilprojekte des Frontends müssen geklont werden.



Die Verlinkung aller Teilprojekte untereinander (außer Cockpit) muss mit npm link hergestellt werden.



Bei der Benutzung von npm link müssen alle Verlinkungen mit einem einzigen Aufruf vorgenommen werden. Beispiel: npm link project1 project2 project3



Das Cockpit muss mit allen Teilprojekten mit npm link verbunden werden.



Damit die Übersetzungen korrekt dargestellt werden können, darf nur eine Instanz der Pakets react-i18-next in dem Projektverbund enthalten sein. Dafür muss in allen Teilprojekten (außer Cockpit) die lokale Instanz von react-i18-next mit der Instanz des Cockpits mit npm link react-i18-next ausgetauscht bzw. verbunden werden.



Im Projektordner vom Cockpit kann der Build-Prozess mit npm run build angestoßen werden. Das kompilierte Frontend mit allen Teilprojekten liegt jetzt im Ordner dist des Cockpits.



Die Definitionsdatei für Maven pom.xml muss editiert werden und alle Platzhalter [HOSTNAME_OR_IP], [PORT], [USERNAME], [PASSWORD] müssen sinnvoll befüllt werden.



Das finale Frontend-Artefakt für das Deployment auf einem JBoss-Server kann mit dem Befehl mvn package erstellt werden.



## Deployment



Das fertige Frontend-Artefakt sollte auf der selben JBoss-Instanz betrieben werden, wie das Backend-Artefakt.



Reverse Proxy Nginx



Es ist für den Aufruf des Editors noch eine Weiterleitung einzurichten. Es ist ein Einrichtungsbeispiel für Nginx angehängt.



```
location ~ /LEA/(.*) {

    set $auth_basic off;

    proxy_pass <url-to-editor;>

}


```





