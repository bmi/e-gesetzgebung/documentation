# Editor Backend

Das Editor Backend stellt REST-Schnittstellen bereit.
Genutzt werden die REST-Endpunkte vom Editor Frontend und von Komponenten der Plattform.

Die Schnittstellenbeschreibungen können in der OpenApi Dokumentation eingesehen werden.
Beispielsweise nach dem Start auf localhost: http://localhost:8080/LEA/API/v1/swagger-ui/index.html

Das Editor Backend ist ein SpringBoot Projekt, das mit maven gebaut wird.
Benötigt wird ein Java Development Kit (JDK) in Version 11 und maven in einer aktuellen Version.

```
mvn package
```

# Abhängigkeiten und benötigte Infrastruktur
Neben Projektabhängigkeiten müssen auch bestimmte Infrastrukturkomponenten bereitstehen.
Die Projektabhängigkeiten müssen bereits während des Kompilierens bereitstehen.
Die Infrastrukturkomponenten und die entsprechenden Konfigurationen werden benötigt umd die Editor-Backend-Komponente starten und damit verwenden zu können.

## Abhängige Projekte zum Kompilieren
#### egg-authorization Project
Das Projekt _egg-authorization_ ist eine Abhängigkeite, die nicht in öffentlich zugänglichen maven Repositories zu finden ist.
Sie muss daher vorher gebaut werden mit
```
maven package
```
und dann im verwendeten maven Repository installiert werden z. B. im lokalen maven repository mit

## Benötigte Infrastruktur
Folgende Infrastrukturkomponenten müssen vorhanden sein, damit das Editor-Backend lauffähig ist und konfiguriert werden kann.
- Datenbank editor
- Datenbank rbac
- IAM (Keycloak)
## Konfiguration
Aufgeführt werden die wichtigsten Elemente der Konfiguration zur Anbindung und Verwendung der Infrastrukturkomponenten.
### Datenbank Editor
In dieser Datenbank werden die Daten der Gesetzgebungstexte, die bearbeiet werden abgespeichert. Hier eine Beispielkonfiguration für eine H2-Datenbank:
```
spring.datasource.url=jdbc:h2:mem:testdb;MODE=MYSQL
spring.datasource.username=
spring.datasource.password=
spring.datasource.driver-class-name=org.h2.Driver
spring.flyway.enabled=true
spring.flyway.locations=classpath:db/migration/{vendor},classpath:db/migration/all
spring.jpa.database-platform=H2
spring.jpa.hibernate.ddl-auto=validate
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.H2Dialect
spring.jpa.properties.hibernate.physical_naming_strategy=org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy
spring.jpa.show-sql=false
spring.sql.init.platform=mysql
```

### Datenbank rbac
Für die Editor Komponente ist ein Rollenbasiertes Berechtigungskonzept (Role Based Access Control - RBAC) umgesetzt.
Die dafür notwendigen Daten werden in einer separaten Datenbank persistiert.
Eine Beispielkonfiguration könnte wie folgt aussehen:
```
auth.datasource.url=jdbc:h2:mem:testdb2;MODE=MYSQL
auth.datasource.username=
auth.datasource.password=
auth.datasource.driver-class-name=org.h2.Driver
auth.flyway.enable=true
auth.flyway.locations=classpath:db/auth/{vendor},classpath:db/auth/all
```

### IAM (Keycloak)
Als Vorraussetzung wird ein Keycloak benötigt. Eine Beispielkonfiguration könnte wie folgt aussehen:
```
spring.security.oauth2.client.provider.keycloak.issuer-uri=keycloak-issuer-uri
spring.security.oauth2.client.provider.keycloak.user-name-attribute=preferred_username
spring.security.oauth2.client.registration.keycloak.authorization-grant-type=authorization_code
spring.security.oauth2.client.registration.keycloak.client-id=clientId
spring.security.oauth2.client.registration.keycloak.client-name=clientName
spring.security.oauth2.client.registration.keycloak.client-secret=clientSecret
spring.security.oauth2.client.registration.keycloak.redirect-uri=redirectURI
spring.security.oauth2.client.registration.keycloak.scope=openid
spring.security.oauth2.client.resourceserver.issuer-uri=keycloak-issuer-uri
spring.security.oauth2.client.resourceserver.jwk-set-uri=keycloak-jwk-uri
```

## Plattform Konfiguration
Die Editor Backend Komponente tauscht mit der Plattform Komponente Daten per REST-Schnittsteleln aus.
Um die von Plattform bereitgestellten REST-Schnittstellena aufrufen zu können, muss konfiguriert werden, wo der Endpunkt zu finden ist.
Abgesichert wird der Datenaustausch über ein API - Token, dass ebenfalls konfiguriert werden muss.
Folgende Konfiguration ist notwendig:
```
plateg.x-api-key=<token>
plateg.url=https://<url>/egesetzgebung-platform-backend
```