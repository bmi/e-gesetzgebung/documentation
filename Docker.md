**ACHTUNG: Diese Funktionalität befindet sich noch in der Entwicklung und bietet noch keine voll funktionsfähige Umgebung.**

# Docker Setup
## Requirements 
- An installation of Docker (or Docker Desktop when working locally) and Docker Compose (included in Docker Desktop)
- Node v16 to build the frontend

## Running the project 
- Clone all repositories from [https://gitlab.opencode.de/bmi/e-gesetzgebung](E-Gesetzgebung) and read through the instructions in the [README](./README.md) and the setup instructions for the [plateg-backend](./Plateg-Setup.md) and the [LEA backend](./LEA-Setup.md). (Most of the steps are implemented in the docker setup, but it's good to know the general instructions to understand the setup)
  - Make sure that all everything is under the same root folder as the `docker-compose.yml` and the `docker` folder
    - If you cloned the `documentation` repo, which contains this file and the necessary docker files, then move the `docker-compose.yml` and the `docker` folder one level up or pull all the other repos into the `documentation` folder.
- Important steps before building: 
  - Fill out the necessary properties described in the README in the respective `application.properties` as described in [lea-editor-backend](https://gitlab.opencode.de/bmi/e-gesetzgebung/lea-editor-backend) and [plateg-backend](https://gitlab.opencode.de/bmi/e-gesetzgebung/plateg-backend)
    - The database will be available under the url `mysql://data:3306`, therefore the respective property needs to be set to (example for `plateg-backend`):
      - `spring.datasource.baseUrl=jdbc:mysql://data:3306` 
    - The IAM will be available under `http://iam:8080`, so set the respective property:
      - `iam.url=http://iam:8080`
  - `npm link` all the frontend modules
    - Check which modules are required in the respective `package.json` and link the respective modules
    - As described in the README the `npm link` must contain all the projects in one call, so `npm link project1 project2 etc.`
    - All projects need to be linked in `plateg-cockpit`
- Building the project: 
  - Most of the building happens in the [Dockerfile for the backend](./docker/plateg-backend/Dockerfile), but the frontend artifact needs the webpack build, so it is necessary to run `npm run build` once in `plateg-cockpit`
    - Make sure to use Node.js v16, otherwise the build will break silently, if used without `--verbose` flag
  - Run `docker compose build` to build the backend and frontend artifacts and create a wildfly image, which contains all the artifacts
  - Run `docker compose up -d` to start the compose setup this will start:
    - A wildfly container, referred to as the `backend` service
    - A keycloak for authentication in the `iam` service
    - A MySQL database in the `data` service
    - A mock for the user management and ressort data in `ressortsapi`
    - A reverse proxy in form of a Caddy in `reverse_proxy` 
      - [Link to the Caddyfile](./docker/reverse_proxy/Caddyfile)

## Property changes
### Plateg Backend
The following changes need to be made in the [`application.properties`](https://gitlab.opencode.de/bmi/e-gesetzgebung/plateg-backend/-/blob/main/src/main/resources/application.properties?ref_type=heads):

- Database:
  - `spring.datasource.baseUrl=jdbc:mysql://data:3306`
  - `spring.datasource.url=${spring.datasource.baseUrl}/eGesetz?useUnicode=yes&characterEncoding=UTF-8`
  - `spring.datasource.username=root`
  - `spring.datasource.password=root`
- IAM:
  - `iam.url=http://iam:8080`
- Editor-URL:
  - `editor.url=http://localhost:8080/cockpit`

### LEA Editor Backend
- `auth.datasource.url=jdbc:h2:mem:testdb2;MODE=MYSQL`
- `auth.datasource.username=`
- `auth.datasource.password=`
- `auth.datasource.driver-class-name=org.h2.Driver`
- `auth.flyway.enable=true`
- `auth.flyway.locations=classpath:db/auth/{vendor},classpath:db/auth/all`
- `spring.security.oauth2.client.provider.keycloak.issuer-uri=keycloak-issuer-uri`
- `spring.security.oauth2.client.provider.keycloak.user-name-attribute=<preferred_username>`
- `spring.security.oauth2.client.registration.keycloak.authorization-grant-type=authorization_code`
- `spring.security.oauth2.client.registration.keycloak.client-id=egg`
- `spring.security.oauth2.client.registration.keycloak.client-name=egg`
- `spring.security.oauth2.client.registration.keycloak.client-secret=<clientSecret>`
- `spring.security.oauth2.client.registration.keycloak.redirect-uri=http://localhost:8080`
- `spring.security.oauth2.client.registration.keycloak.scope=openid`
- `spring.security.oauth2.client.resourceserver.issuer-uri=<keycloak-issuer-uri>`
- `spring.security.oauth2.client.resourceserver.jwk-set-uri=<keycloak-jwk-uri>`
- `plateg.x-api-key=<token>`
- `plateg.url=http://localhost:8080/egesetzgebung-platform-backend`
